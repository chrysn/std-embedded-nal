/// Wrapper around the `core` IP address type that converts to `nix` types and vice versa.
#[derive(Debug, Clone, Copy)]
pub(crate) struct IpAddr(pub(crate) core::net::IpAddr);

// That this is missing zone info on IPv6 probably just makes core::net::IpAddr a bad intermediate
// type.
impl From<nix::libc::in6_pktinfo> for IpAddr {
    fn from(input: nix::libc::in6_pktinfo) -> Self {
        // FIXME why isn't this having zone infos??
        Self(input.ipi6_addr.s6_addr.into())
    }
}

impl From<IpAddr> for nix::libc::in6_pktinfo {
    fn from(input: IpAddr) -> nix::libc::in6_pktinfo {
        let input = match input.0 {
            core::net::IpAddr::V6(a) => a,
            _ => panic!("Type requires IPv6 addresses"),
        };
        nix::libc::in6_pktinfo {
            ipi6_addr: nix::libc::in6_addr {
                s6_addr: input.octets(),
            },
            // FIXME and here it really hurts
            ipi6_ifindex: 0,
        }
    }
}

impl From<nix::libc::in_pktinfo> for IpAddr {
    fn from(input: nix::libc::in_pktinfo) -> Self {
        // FIXME discarding interface index?
        Self(core::net::Ipv4Addr::from(input.ipi_spec_dst.s_addr).into())
    }
}

impl From<IpAddr> for nix::libc::in_pktinfo {
    fn from(input: IpAddr) -> nix::libc::in_pktinfo {
        let input = match input.0 {
            core::net::IpAddr::V4(a) => a,
            _ => panic!("Type requires IPv4 addresses"),
        };
        nix::libc::in_pktinfo {
            ipi_spec_dst: nix::libc::in_addr {
                s_addr: input.into(),
            },
            ipi_addr: nix::libc::in_addr {
                s_addr: input.into(),
            },
            // FIXME and here it really hurts
            ipi_ifindex: 0,
        }
    }
}
