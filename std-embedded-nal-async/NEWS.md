# Changes in 0.3.0

* embedded-nal dependency changed from 0.7 to 0.8.
* Bump MSRV to 1.77.
* Removed explicit lifetime annotation from TcpConnect::connect.
* README and metadata fixes.

# Changes in 0.2.0

* embedded-nal-async dependency changed from 0.6 to 0.7.

  The one user visible change is the update of the `Dns::get_host_by_address` signature,
