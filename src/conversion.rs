use embedded_nal::nb;
use std::io;

pub(crate) fn to_nb(e: io::Error) -> nb::Error<io::Error> {
    use io::ErrorKind::{TimedOut, WouldBlock};
    match e.kind() {
        WouldBlock | TimedOut => nb::Error::WouldBlock,
        _ => e.into(),
    }
}
